#include <bmp.h>
#include <stdio.h>

int main() {
    uint8_t *data = NULL;
    BITMAPFILEHEADER file_header;
    BITMAPINFOHEADER header;
    RGBQUAD *color_table_origin = NULL, *color_table = NULL;

    read_bmp("resources/a.bmp", &data, &file_header, &header,
             &color_table_origin);
    write_bmp("resources/b.bmp", data, &file_header, &header,
              color_table_origin);

    rgb_to_gray(&data, &file_header, &header, &color_table);
    write_bmp("resources/c.bmp", data, &file_header, &header, color_table);

    linear_expanding(&data, &header, 0, 255, 50, 150);
    write_bmp("resources/d.bmp", data, &file_header, &header, color_table);

    return 0;
}
