#include <bmp.h>
#include <stdio.h>

int main() {
    uint8_t *data = NULL;
    BITMAPFILEHEADER file_header;
    BITMAPINFOHEADER header;
    RGBQUAD *color_table_origin = NULL;
    float histogram[256] = {0.0f};
    int i;

    read_bmp("resources/c.bmp", &data, &file_header, &header,
             &color_table_origin);
    histogram_256(header.bi_width, header.bi_height, data, histogram);
    // FILE *fp = fopen("histogram.data", "w");
    // for (i = 0; i < 256; ++i) {
    //     fprintf(fp, "%f ", histogram[i]);
    // }
    // fclose(fp);

    return 0;
}
