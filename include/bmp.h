#ifndef __BMP_H__
#define __BMP_H__

/**
 * BMP image reader and writer.
 */

#include <assert.h>
#include <malloc.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#pragma pack(push)
#pragma pack(1)

typedef struct tagBITMAPFILEHEADER {
    uint16_t bf_type;       // 0x424D "BM".
    uint32_t bf_size;       // the size of BMP file in bytes.
    uint16_t bf_reserved1;  // 0
    uint16_t bf_reserved2;  // 0
    uint32_t bf_off_bits;   // The offset, of the byte where the bitmap image
                            // data (pixel array) can be found.
} BITMAPFILEHEADER;

typedef struct tagBITMAPINFOHEADER {
    uint32_t bi_size;       // size of struct BITMAPINFOHEADER.
    int32_t bi_width;       // width of image, in pixel.
    int32_t bi_height;      // height of image.
    uint16_t bi_planes;     // the number of color planes (must be 1).
    uint16_t bi_bit_count;  // the number of bits per pixel, which is the color
                            // depth of the image. Typical values are 1, 4, 8,
                            // 16, 24 and 32.
    uint32_t bi_compression;  // the compression method being used.
    uint32_t bi_size_image;   // the image size. This is the size of the raw
                              // bitmap data; a dummy 0 can be given for BI_RGB
                              // bitmaps.
    int32_t bi_x_pels;     // the horizontal resolution of the image, pixel per
                           // meter.
    int32_t bi_y_pels;     // the vertical resolution of the image, pixel per
                           // meter.
    uint32_t bi_clr_used;  // the number of colors in the color palette, or 0 to
                           // default to 2n.
    uint32_t bi_clr_important;  // the number of important colors used, or 0
                                // when every color is important; generally
                                // ignored.
} BITMAPINFOHEADER;

typedef struct tagRGBQUAD {
    uint8_t rgb_b;  // blue
    uint8_t rgb_g;  // green
    uint8_t rgb_r;  // red
    uint8_t reb_reserved;
} RGBQUAD;

typedef struct tagBITMAPINFO {
    BITMAPINFOHEADER bmi_header;
    // the number of entries in color table is decided by the value of
    // bi_bit_count field.
    //      bi_bit_count = 1: 2
    //      bi_bit_count = 4: 16
    //      bi_bit_count = 8: 256
    //      otherwise    = 0, no color table entry.
    RGBQUAD bmi_colors[1];
} BITMAPINFO;

#pragma pack(pop)

// Read and write BMP image file.
void read_bmp(const char *fname, uint8_t **data, BITMAPFILEHEADER *file_header,
              BITMAPINFOHEADER *header, RGBQUAD **color_table);
void write_bmp(const char *fname, const uint8_t *data,
               BITMAPFILEHEADER *file_header, BITMAPINFOHEADER *header,
               RGBQUAD *color_table);
void rgb_to_gray(uint8_t **data, BITMAPFILEHEADER *file_header,
                 BITMAPINFOHEADER *header, RGBQUAD **color_table);

// Calculate gray distribution of image data, use 256 gray color levels.
void histogram_256(int w, int h, const uint8_t *data, float dest[]);
// Gray histogram equalize.
void histogram_equalize(int w, int h, uint8_t **image, const float *histogram);

// Gray expanding.
void linear_expanding(uint8_t **data, const BITMAPINFOHEADER *header, int m1,
                      int m2, int n1, int n2);
void log_expanding(uint8_t **data, const BITMAPINFOHEADER *header, double a,
                   double b, double c);

#endif /* __BMP_H__ */
