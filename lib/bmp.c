#include <bmp.h>

size_t calc_color_table_entries(size_t bmp_bit_count) {
    switch (bmp_bit_count) {
        case 1:
            return 2;
        case 4:
            return 16;
        case 8:
            return 256;
        default:
            return 0;
    }
}

// Allocate space for data, use two-dimension array to store image data.
void read_bmp(const char *fname, uint8_t **data, BITMAPFILEHEADER *file_header,
              BITMAPINFOHEADER *header, RGBQUAD **color_table) {
    FILE *fp = fopen(fname, "rb");
    assert(fp != NULL);
    int bmp_width, bmp_height, bmp_bit_count;
    int line_bytes, data_bytes, color_table_entries;

    assert(sizeof(BITMAPFILEHEADER) == 14);
    assert(sizeof(BITMAPINFOHEADER) == 40);

    // read the file header.
    fread(file_header, sizeof(BITMAPFILEHEADER), 1, fp);

    // read information header.
    fread(header, sizeof(BITMAPINFOHEADER), 1, fp);
    bmp_width = header->bi_width;
    bmp_height = header->bi_height;
    bmp_bit_count = header->bi_bit_count;
    line_bytes = (bmp_width * bmp_bit_count / 8 + 3) / 4 * 4;  // align.
    data_bytes = line_bytes * bmp_height;

    // read color table.
    color_table_entries = calc_color_table_entries(bmp_bit_count);
    assert(*color_table == NULL);
    *color_table = (RGBQUAD *)malloc(color_table_entries * sizeof(RGBQUAD));
    fread(*color_table, sizeof(RGBQUAD), color_table_entries, fp);

    // read image data.
    assert(*data == NULL);
    *data = (uint8_t *)malloc(data_bytes);
    fread(*data, data_bytes, 1, fp);
    fclose(fp);
}

// Use two-dimension array to store image data.
void write_bmp(const char *fname, const uint8_t *data,
               BITMAPFILEHEADER *file_header, BITMAPINFOHEADER *header,
               RGBQUAD *color_table) {
    assert(data != NULL);

    FILE *fp = fopen(fname, "wb");
    assert(fp != NULL);
    int data_bytes, color_table_entries;

    data_bytes = header->bi_size_image;
    color_table_entries = calc_color_table_entries(header->bi_bit_count);

    // validate and write file header.
    assert(file_header->bf_type == 0x4D42);  // magic flag.
    assert(file_header->bf_off_bits = sizeof(BITMAPFILEHEADER) +
                                      sizeof(BITMAPINFOHEADER) +
                                      color_table_entries * 4);
    assert(file_header->bf_size == file_header->bf_off_bits + data_bytes);
    assert(file_header->bf_reserved1 == 0);
    assert(file_header->bf_reserved2 == 0);
    fwrite(file_header, sizeof(BITMAPFILEHEADER), 1, fp);

    // write information header.
    assert(header->bi_size = sizeof(BITMAPINFOHEADER));
    fwrite(header, sizeof(BITMAPINFOHEADER), 1, fp);

    // write color table.
    fwrite(color_table, sizeof(RGBQUAD), color_table_entries, fp);

    // write image data.
    fwrite(data, data_bytes, 1, fp);

    fclose(fp);
}

// Update the origin data and headers directly, and make new color_table.
void rgb_to_gray(uint8_t **data, BITMAPFILEHEADER *file_header,
                 BITMAPINFOHEADER *header, RGBQUAD **color_table) {
    int data_bytes = header->bi_size_image;
    size_t i, color_table_entries;
    uint8_t *t = NULL;

    assert(header->bi_bit_count == 24);  // RGB image.

    header->bi_bit_count = 8;
    header->bi_size_image = (header->bi_width + 3) / 4 * 4 * header->bi_height;

    color_table_entries = calc_color_table_entries(header->bi_bit_count);
    assert(*color_table == NULL);
    *color_table = (RGBQUAD *)malloc(color_table_entries * sizeof(RGBQUAD));
    for (i = 0; i < color_table_entries; ++i) {
        (*color_table)[i].rgb_r = i;
        (*color_table)[i].rgb_g = i;
        (*color_table)[i].rgb_b = i;
        (*color_table)[i].reb_reserved = 0;
    }

    file_header->bf_off_bits = sizeof(BITMAPFILEHEADER) +
                               sizeof(BITMAPINFOHEADER) +
                               color_table_entries * sizeof(RGBQUAD);
    file_header->bf_size = file_header->bf_off_bits + header->bi_size_image;

    assert(data_bytes / header->bi_size_image == 3);

    t = (uint8_t *)malloc(data_bytes);
    memcpy(t, *data, data_bytes);
    free(*data);
    *data = NULL;
    *data = (uint8_t *)malloc(header->bi_size_image);
    for (i = 0; i < header->bi_size_image; ++i) {
        (*data)[i] = (uint8_t)((float)t[i * 3 + 0] * 0.299 +
                               (float)t[i * 3 + 1] * 0.587 +
                               (float)t[i * 3 + 2] * 0.114);
    }
}

// Calculate gray distribution of image data, use 256 gray color levels.
void histogram_256(int w, int h, const uint8_t *data, float dest[]) {
    // const uint8_t level = 256U
    int i, nk[256] = {0};

    assert(data != NULL);

    for (i = 0; i < w * h; ++i) {
        nk[data[i]] += 1;
    }
    for (i = 0; i < 256; ++i) {
        dest[i] = (float)nk[i] / (w * h);
    }
    // FINISH, the data of gray histogram is stored in (*dest).
}

// Gray histogram equalize.
void histogram_equalize(int w, int h, uint8_t **image, const float *histogram) {
    // const uint8_t level = 256;
    float acc_distr[256] = {0.0};
    uint8_t cumu_pixel[256] = {0};
    int i;

    acc_distr[0] = histogram[0];
    for (i = 1; i < 256; ++i) {
        acc_distr[i] = acc_distr[i - 1] + histogram[i];
    }
    for (i = 0; i < 256; ++i) {
        cumu_pixel[i] = (uint8_t)(255.0 * acc_distr[i] + 0.5);
    }
    for (i = 0; i < w * h; ++i) {
        (*image)[i] = cumu_pixel[(*image)[i]];
    }
}

// Linear gray expanding.
void linear_expanding(uint8_t **data, const BITMAPINFOHEADER *header, int m1,
                      int m2, int n1, int n2) {
    size_t i;
    uint8_t *t = NULL;

    assert(header->bi_bit_count == 8);  // gray image.
    assert(t == NULL);
    t = (uint8_t *)malloc(header->bi_size_image);
    memcpy(t, *data, header->bi_size_image);
    free(*data);
    *data = NULL;
    assert(*data == NULL);
    *data = (uint8_t *)malloc(header->bi_size_image);

    for (i = 0; i < header->bi_size_image; ++i) {
        if ((*data)[i] <= m2 && (*data)[i] >= m1) {
            (*data)[i] = (float)(n2 - n1) / (m2 - m1) * (t[i] - m1) + n1;
        } else {
            (*data)[i] = t[i];
        }
    }
}

// Logarithm gray expanding.
// g(x, y) = a + \frac{\ln{[f(x, y)+1]}}{b * \ln{c}}
void log_expanding(uint8_t **data, const BITMAPINFOHEADER *header, double a,
                   double b, double c) {
    size_t i;
    uint8_t *t = NULL;

    assert(header->bi_bit_count == 8);  // gray image.
    assert(t == NULL);
    t = (uint8_t *)malloc(header->bi_size_image);
    memcpy(t, *data, header->bi_size_image);
    free(*data);
    *data = NULL;
    assert(*data == NULL);
    *data = (uint8_t *)malloc(header->bi_size_image);

    for (i = 0; i < header->bi_size_image; ++i) {
        (*data)[i] = a + log((double)t[i] + 1) / (b * log(c));
    }
}
